let email = document.querySelector(".email");
let pw = document.querySelector(".pw");


// validating email
email.addEventListener("change", function(){
    let emailSpan = document.querySelector(".emailSpan");
    emailString = email.value;
    lenthOfEmailString = emailString.length;
    com = emailString.substring(lenthOfEmailString-3);

    
    if(!(com === 'com')){
        let textNode = document.createTextNode("enter a valid email address");
        emailSpan.appendChild(textNode);
        console.log('invalid')
    }
    else {
        
        emailSpan.innerHTML = "";
        console.log('valid')
    }
})

// validating pw

pw.addEventListener("change", function(){
    let pwString = pw.value;
    console.log(pwString);
    let pwLen = pwString.length;
  
    haveUpp = false;
    haveLow = false;
    haveDig = false;

    for( i = 0; i < pwLen; i++){
        ch = pwString.charAt(i);
        // console.log(ch);
        if(ch >= 'A' && ch <= 'Z'){
            haveUpp = true;
        }else if(ch >= 'a' && ch <= 'z'){
            haveLow = true;
        }else if(ch >= '0' && ch <= '9'){
            haveDig = true;
        }

    }

    let pwSpan = document.querySelector(".pwSpan");
    if(pwLen < 6 || !(haveUpp) || !(haveLow) || !(haveDig)){
        let pwSpanText = document.createTextNode("enter password of min 6 lenght, altleast one upper case and one lowercase letter");
        pwSpan.appendChild(pwSpanText);
    }else{
        pwSpan.innerHTML = "";
    }

})

// selecting radion button
let roleDiv = document.querySelector(".role");
let roleArr = document.getElementsByName("role");

let roleCount = 0;
roleDiv.addEventListener("click", function(){
    for(let i = 0; i < roleArr.length; i++){
        if(roleArr[i].checked){
            console.log(roleArr[i].value);
            roleCount++;
        }
    }

    

})

// permissions
let perDiv = document.querySelector(".permission");
let perCheck = document.querySelectorAll(".perm");

checkCount = 0;
perDiv.addEventListener('click', function(){
    for(let i = 0; i < perCheck.length; i++){
        if(perCheck[i].checked){
            checkCount++;
            console.log(perCheck[i].value);
        }
    }

    // warning for permission to check atleast two
    let permSpan = document.querySelector(".permSpan");
    if(checkCount < 2 && permSpan.innerHTML == "" ){
        console.log("select atleast two permission");
        // let permSpan = document.querySelector(".permSpan");
        let permText = document.createTextNode("Please select atleast two permissions");
        permSpan.appendChild(permText);
    }
    else{
        
        permSpan.innerHTML = "";
    }
})

// handling submit button
let submit = document.querySelector(".submit");
submit.addEventListener("click", function(e){
    e.preventDefault();
    let allWarningSpan = document.querySelector(".allWarningSpan");
    isAllFill = true;

    let allWarningSpanText = allWarningSpan.innerHTML;
    

    if(email.value == "" && allWarningSpanText == "" ){
        let textNode = document.createTextNode("Fill all details");
        allWarningSpan.appendChild(textNode);
        isAllFill = false
    }
    else if(pw.value == ""  && allWarningSpanText == ""){
        let textNode = document.createTextNode("Fill all details");
        allWarningSpan.appendChild(textNode);
        isAllFill = false
    }
    else if(roleCount == 0  && allWarningSpanText == ""){
        let textNode = document.createTextNode("Fill all details");
        allWarningSpan.appendChild(textNode);
        isAllFill = false
    }
    else if(checkCount == 0  && allWarningSpanText == ""){
        let textNode = document.createTextNode("Fill all details");
        allWarningSpan.appendChild(textNode);
        isAllFill = false
    }
    
    if(isAllFill){
        submit.remove();

        // create confirm button
        let confirmButton = document.createElement("BUTTON");
        var confirmButtonText = document.createTextNode("Confirm");
        confirmButton.classList.add("submit");
        confirmButton.appendChild(confirmButtonText);

        let form = document.querySelector(".form");
        form.appendChild(confirmButton);
        document.body.appendChild(form);
    }
})